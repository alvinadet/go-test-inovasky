package controllers

import (
	"go-fiber-starter/app/dto"
	"go-fiber-starter/app/services"
	"go-fiber-starter/utils"

	"github.com/gofiber/fiber/v2"
)

type MemberController struct{}

func (c *MemberController) memberService() *services.MemberService {
	return new(services.MemberService)
}

func (c *MemberController) List(ctx *fiber.Ctx) error {
	utils.Logger.Info("✅ MEMBER LIST")

	paginate := utils.GetPaginationParams(ctx)
	return c.memberService().List(ctx, paginate)
}

func (c *MemberController) Add(ctx *fiber.Ctx) error {
	utils.Logger.Info("✅ MEMBER  ADD")

	req := new(dto.MemberDTO)
	if err := ctx.BodyParser(req); err != nil {
		return utils.JsonErrorValidation(ctx, err)
	}

	return c.memberService().Add(ctx, *req)
}
