package controllers

import (
	"go-fiber-starter/app/dto"
	"go-fiber-starter/app/services"
	"go-fiber-starter/utils"

	"github.com/gofiber/fiber/v2"
)

type MemberTypeController struct{}

func (c *MemberTypeController) memberTypeService() *services.MemberTypeService {
	return new(services.MemberTypeService)
}

func (c *MemberTypeController) List(ctx *fiber.Ctx) error {
	utils.Logger.Info("✅ MEMBER TYPE LIST")

	paginate := utils.GetPaginationParams(ctx)
	return c.memberTypeService().List(ctx, paginate)
}

func (c *MemberTypeController) Detail(ctx *fiber.Ctx) error {
	utils.Logger.Info("✅ MEMBER TYPE LIST")

	guid, err := utils.ValidateGUIDParams(ctx)
	if err != nil {
		return err
	}

	return c.memberTypeService().Detail(ctx, guid)
}

func (c *MemberTypeController) Add(ctx *fiber.Ctx) error {
	utils.Logger.Info("✅ MEMBER TYPE ADD")

	req := new(dto.MemberTypeDTO)
	if err := ctx.BodyParser(req); err != nil {
		return utils.JsonErrorValidation(ctx, err)
	}

	return c.memberTypeService().Add(ctx, *req)
}

func (c *MemberTypeController) Update(ctx *fiber.Ctx) error {
	utils.Logger.Info("✅ MEMBER TYPE UPDATE")

	guid, err := utils.ValidateGUIDParams(ctx)
	if err != nil {
		return err
	}

	req := new(dto.MemberTypeDTO)
	if err := ctx.BodyParser(req); err != nil {
		return utils.JsonErrorValidation(ctx, err)
	}

	return c.memberTypeService().Update(ctx, guid, *req)
}

func (c *MemberTypeController) Delete(ctx *fiber.Ctx) error {
	utils.Logger.Info("✅ MEMBER TYPE UPDATE")

	guid, err := utils.ValidateGUIDParams(ctx)
	if err != nil {
		return err
	}

	return c.memberTypeService().Delete(ctx, guid)
}
