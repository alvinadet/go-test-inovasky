package dto

import (
	"go-fiber-starter/utils"

	"github.com/google/uuid"
)

type MemberDTO struct {
	Name           string    `json:"name" validate:"required"`
	Email          string    `json:"email"`
	PhoneNumber    string    `json:"phone_number"`
	PhotoProfile   string    `json:"photo_profile"`
	MemberTypeGUID uuid.UUID `json:"member_type_guid"`
	Password       string    `json:"password"`
	Balance        float64   `json:"balance" validate:"numeric,min=0"`
}

func (req *MemberDTO) Validate() error {
	return utils.ExtractValidationError(req)
}

type MemberAddConsumerDTO struct {
	Name           string  `mapstructure:"name"`
	Email          string  `mapstructure:"email"`
	Password       string  `mapstructure:"password"`
	PhoneNumber    string  `mapstructure:"phoneNumber"`
	MemberTypeUUID string  `mapstructure:"member_type_guid"`
	PhotoProfile   string  `mapstructure:"photo_profile"`
	Balance        float64 `mapstructure:"balance"`
}
