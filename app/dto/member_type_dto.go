package dto

import "go-fiber-starter/utils"

type MemberTypeDTO struct {
	Name string `json:"name" validate:"required"`
}

func (req *MemberTypeDTO) Validate() error {
	return utils.ExtractValidationError(req)
}
