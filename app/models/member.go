package models

import (
	"time"

	"github.com/google/uuid"
)

type Member struct {
	GUID            uuid.UUID  `json:"guid" gorm:"primaryKey;type:uuid;default:gen_random_uuid()"`
	Name            string     `json:"name" gorm:"type:varchar"`
	Email           string     `json:"email" gorm:"type:varchar;uniqueIndex:unique_idx_user"`
	Password        string     `json:"password" gorm:"type:varchar"`
	PhoneNumber     string     `json:"phone_number" gorm:"type:varchar"`
	PhotoProfile    string     `json:"photo_profile" gorm:"type:varchar"`
	Balance         float64    `json:"balance" grom:"type:decimal"`
	CreatedAt       time.Time  `json:"created_at"`
	UpdatedAt       time.Time  `json:"updated_at"`
	MemberTypeGUID  uuid.UUID  `json:"member_type_guid" gorm:"name:member_type;type:uuid REFERENCES MemberType(GUID)"`
	MemberTypeRefer MemberType `gorm:"ForeignKey:MemberTypeGUID;AssociationForeignKey:GUID"`
}

func (m *Member) TableName() string {
	return "member"
}
