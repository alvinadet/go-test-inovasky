package models

import (
	"time"

	"github.com/google/uuid"
)

type MemberType struct {
	GUID      uuid.UUID `json:"guid" gorm:"primaryKey;type:uuid;default:gen_random_uuid()"`
	Name      string    `json:"name" gorm:"type:varchar"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (m *MemberType) TableName() string {
	return "member_type"
}
