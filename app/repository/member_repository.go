package repository

import (
	"go-fiber-starter/app/models"
	"go-fiber-starter/utils"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type MemberRepository struct{}

func (r *MemberRepository) filterClauses(pagination *utils.Pagination) (clauses []clause.Expression) {
	if pagination.Keyword != "" {
		vars := setKeywordVarsByTotalExpr(pagination.Keyword, 3)
		query := lowerLikeQuery("name") + "OR" + lowerLikeQuery("phone_number") + "OR" + "OR" + lowerLikeQuery("email")
		clauses = append(clauses, clause.Expr{SQL: query, Vars: vars})
	}

	return clauses

}

func (r *MemberRepository) GetAll(pagination utils.Pagination) (*utils.Pagination, error) {
	var members []*models.Member
	clause := r.filterClauses(&pagination)
	filter := filterPaginate(members, &pagination, clause)

	if err := DB.Scopes(filter).Find(&members).Error; err != nil {
		return nil, err
	}

	pagination.Rows = members

	return &pagination, nil
}

func (r *MemberRepository) memberTypeRepo() *MemberTypeRepository {
	return new(MemberTypeRepository)
}

func (r *MemberRepository) Insert(tx *gorm.DB, Member models.Member) (models.Member, error) {

	if _, err_member_type := r.memberTypeRepo().FindByGUID(Member.MemberTypeGUID.String()); err_member_type != nil {
		return Member, err_member_type
	}

	if err := tx.Create(&Member).Error; err != nil {
		return Member, err
	}

	return Member, nil
}
