package repository

import (
	"go-fiber-starter/app/models"
	"go-fiber-starter/utils"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type MemberTypeRepository struct{}

func (r *MemberTypeRepository) filterClauses(pagination *utils.Pagination) (clauses []clause.Expression) {
	if pagination.Keyword != "" {

		vars := setKeywordVarsByTotalExpr(pagination.Keyword, 1)
		query := lowerLikeQuery("name")

		clauses = append(clauses, clause.Expr{SQL: query, Vars: vars})
	}

	return clauses

}

func (r *MemberTypeRepository) GetAll(pagination utils.Pagination) (*utils.Pagination, error) {
	var memberTypes []*models.MemberType
	clause := r.filterClauses(&pagination)
	filter := filterPaginate(memberTypes, &pagination, clause)

	if err := DB.Scopes(filter).Find(&memberTypes).Error; err != nil {
		return nil, err
	}

	pagination.Rows = memberTypes

	return &pagination, nil
}

func (r *MemberTypeRepository) FindByGUID(guid string) (MemberType models.MemberType, err error) {

	if err := DB.First(&MemberType, "guid = ?", guid).Error; err != nil {
		return MemberType, err
	}

	return MemberType, nil
}

func (r *MemberTypeRepository) Insert(tx *gorm.DB, MemberType models.MemberType) (models.MemberType, error) {
	if err := tx.Create(&MemberType).Error; err != nil {
		return MemberType, err
	}

	return MemberType, nil
}

func (r *MemberTypeRepository) UpdateByGUID(tx *gorm.DB, guid string, storeData models.MemberType) (models.MemberType, error) {
	var MemberType models.MemberType
	if err := tx.Model(&MemberType).Where("guid = ?", guid).Updates(&storeData).Error; err != nil {
		return MemberType, err
	}

	return MemberType, nil
}

func (r *MemberTypeRepository) DeleteByGUID(guid string) error {
	var MemberType models.MemberType
	if err := DB.Where("guid = ?", guid).Delete(&MemberType).Error; err != nil {
		return err
	}

	return nil
}
