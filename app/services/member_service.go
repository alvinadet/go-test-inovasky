package services

import (
	"encoding/json"
	"go-fiber-starter/app/dto"
	"go-fiber-starter/app/jobs"
	"go-fiber-starter/app/models"
	"go-fiber-starter/app/repository"
	"go-fiber-starter/app/transformer"
	"go-fiber-starter/utils"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/mitchellh/mapstructure"
)

type MemberService struct{}

func (s *MemberService) memberRepo() *repository.MemberRepository {
	return new(repository.MemberRepository)
}

func (s *MemberService) List(ctx *fiber.Ctx, paginate utils.Pagination) error {
	paginationData, err := s.memberRepo().GetAll(paginate)
	if err != nil {
		return utils.JsonErrorInternal(ctx, err, "E_MEMBER_LIST")
	}

	member := paginationData.Rows.([]*models.Member)
	paginationData.Rows = transformer.MemberListTransformer(member)

	return utils.JsonPagination(ctx, paginationData)
}

func (s *MemberService) Add(ctx *fiber.Ctx, req dto.MemberDTO) (err error) {
	if err := req.Validate(); err != nil {
		return utils.JsonErrorValidation(ctx, err)
	}

	memberTypeRepo := s.memberRepo()

	storeData := s.storeFromRequest(models.Member{}, req)
	memberType, err := memberTypeRepo.Insert(repository.DB, storeData)
	if err != nil {
		return utils.JsonErrorInternal(ctx, err, "E_MEMBER_ADD")
	}

	return utils.JsonSuccess(ctx, transformer.MemberDetailTransformer(memberType))
}

func (s *MemberService) AddUserFromConsumer(data []byte) error {
	var MemberAddConsumerDTO dto.MemberAddConsumerDTO

	payload := &jobs.DefaultJobPayloads{}
	json.Unmarshal(data, &payload)

	if err := mapstructure.Decode(payload.Data, &MemberAddConsumerDTO); err != nil {
		return err
	}

	member := models.Member{
		Email:          MemberAddConsumerDTO.Email,
		Name:           MemberAddConsumerDTO.Name,
		Password:       MemberAddConsumerDTO.Password,
		PhoneNumber:    MemberAddConsumerDTO.PhoneNumber,
		PhotoProfile:   MemberAddConsumerDTO.PhotoProfile,
		MemberTypeGUID: uuid.MustParse(MemberAddConsumerDTO.MemberTypeUUID),
		Balance:        MemberAddConsumerDTO.Balance,
	}

	if _, err := s.memberRepo().Insert(repository.DB, member); err != nil {
		return err
	}
	utils.Logger.Info("✅ Create Member : " + MemberAddConsumerDTO.Name)

	return nil
}

func (s *MemberService) storeFromRequest(Member models.Member, req dto.MemberDTO) models.Member {
	Member.Name = req.Name
	Member.Email = req.Email
	Member.MemberTypeGUID = req.MemberTypeGUID
	Member.PhoneNumber = req.PhoneNumber
	Member.Password = req.Password
	Member.PhotoProfile = req.PhotoProfile
	Member.Balance = req.Balance
	return Member
}
