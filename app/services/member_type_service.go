package services

import (
	"go-fiber-starter/app/dto"
	"go-fiber-starter/app/models"
	"go-fiber-starter/app/repository"
	"go-fiber-starter/app/transformer"
	"go-fiber-starter/utils"

	"github.com/gofiber/fiber/v2"
)

type MemberTypeService struct{}

func (s *MemberTypeService) memberTypeRepo() *repository.MemberTypeRepository {
	return new(repository.MemberTypeRepository)
}

func (s *MemberTypeService) List(ctx *fiber.Ctx, paginate utils.Pagination) error {
	paginationData, err := s.memberTypeRepo().GetAll(paginate)
	if err != nil {
		return utils.JsonErrorInternal(ctx, err, "E_MEMBER_TYPE_LIST")
	}

	memberTypes := paginationData.Rows.([]*models.MemberType)
	paginationData.Rows = transformer.MemberTypeTransformer(memberTypes)

	return utils.JsonPagination(ctx, paginationData)
}

func (s *MemberTypeService) Detail(ctx *fiber.Ctx, guid string) error {
	memberType, err := s.memberTypeRepo().FindByGUID(guid)
	if err != nil {
		return utils.JsonErrorInternal(ctx, err, "E_MEMBER_TYPE_DETAIL")
	}

	row := transformer.MemberTypeListResponse(memberType)
	return utils.JsonSuccess(ctx, row)
}

func (s *MemberTypeService) Add(ctx *fiber.Ctx, req dto.MemberTypeDTO) (err error) {
	if err := req.Validate(); err != nil {
		return utils.JsonErrorValidation(ctx, err)
	}

	memberTypeRepo := s.memberTypeRepo()

	storeData := s.storeFromRequest(models.MemberType{}, req)
	memberType, err := memberTypeRepo.Insert(repository.DB, storeData)
	if err != nil {
		return utils.JsonErrorInternal(ctx, err, "E_MEMBER_TYPE_ADD")
	}

	return utils.JsonSuccess(ctx, memberType)
}

func (s *MemberTypeService) Update(ctx *fiber.Ctx, GUID string, req dto.MemberTypeDTO) error {
	if err := req.Validate(); err != nil {
		return utils.JsonErrorValidation(ctx, err)
	}

	memberTypeRepo := s.memberTypeRepo()

	memberType, err := memberTypeRepo.FindByGUID(GUID)
	if err != nil {
		return utils.JsonErrorNotFound(ctx, err)
	}

	storeData := s.storeFromRequest(memberType, req)
	updatedMemberType, err := memberTypeRepo.UpdateByGUID(repository.DB, GUID, storeData)
	if err != nil {
		return utils.JsonErrorInternal(ctx, err, "E_MEMBER_TYPE")
	}

	updatedMemberType.GUID = memberType.GUID

	return utils.JsonSuccess(ctx, updatedMemberType)
}

func (s *MemberTypeService) Delete(ctx *fiber.Ctx, guid string) error {
	if err := s.memberTypeRepo().DeleteByGUID(guid); err != nil {
		return utils.JsonErrorInternal(ctx, err, "E_MEMBER_TYPE_DELETE")
	}

	resData := map[string]interface{}{
		"guid": guid,
	}

	return utils.JsonSuccess(ctx, resData)
}

func (s *MemberTypeService) storeFromRequest(MemberType models.MemberType, req dto.MemberTypeDTO) models.MemberType {
	MemberType.Name = req.Name
	return MemberType
}
