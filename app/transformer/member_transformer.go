package transformer

import (
	"encoding/json"
	"go-fiber-starter/app/models"
	"time"

	"github.com/google/uuid"
)

type MemberListResponse struct {
	GUID         uuid.UUID `json:"guid"`
	Name         string    `json:"name"`
	Email        string    `json:"email"`
	PhoneNumber  string    `json:"phone_number"`
	PhotoProfile string    `json:"photo_profile"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
	MemberType   uuid.UUID `json:"member_type_guid"`
	Balance      float64   `json:"balance"`
}

func MemberListTransformer(Members []*models.Member) (rows []MemberListResponse) {
	for _, row := range Members {
		var mapResponse MemberListResponse
		jsonResponse, _ := json.Marshal(row)
		json.Unmarshal(jsonResponse, &mapResponse)

		rows = append(rows, mapResponse)
	}
	return rows
}

func MemberDetailTransformer(Member models.Member) (data MemberListResponse) {
	data.CreatedAt = Member.CreatedAt
	data.Email = Member.Email
	data.GUID = Member.GUID
	data.MemberType = Member.MemberTypeGUID
	data.Name = Member.Name
	data.PhoneNumber = Member.PhoneNumber
	data.PhotoProfile = Member.PhotoProfile
	data.UpdatedAt = Member.UpdatedAt
	data.Balance = Member.Balance

	return data
}
