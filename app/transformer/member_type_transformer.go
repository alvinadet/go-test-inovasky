package transformer

import (
	"encoding/json"
	"go-fiber-starter/app/models"
	"time"

	"github.com/google/uuid"
)

type MemberTypeListResponse struct {
	GUID      uuid.UUID `json:"guid"`
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func MemberTypeTransformer(MemberTypes []*models.MemberType) (rows []MemberTypeListResponse) {
	for _, row := range MemberTypes {
		var mapResponse MemberTypeListResponse
		jsonResponse, _ := json.Marshal(row)
		json.Unmarshal(jsonResponse, &mapResponse)

		rows = append(rows, mapResponse)
	}
	return rows
}
