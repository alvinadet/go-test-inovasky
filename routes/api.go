package routes

import (
	"go-fiber-starter/app/controllers"

	"github.com/gofiber/fiber/v2"
)

func Setup(app *fiber.App) {
	apiRoute := app.Group("/api")

	// MEMBER TYPE ROUTES
	memberType := apiRoute.Group("/member_type")
	memberTypeController := new(controllers.MemberTypeController)
	memberType.Get("/", memberTypeController.List)
	memberType.Get("/:guid", memberTypeController.Detail)
	memberType.Post("/", memberTypeController.Add)
	memberType.Put("/:guid", memberTypeController.Update)
	memberType.Delete("/:guid", memberTypeController.Delete)

	// MEMBER ROUTES
	member := apiRoute.Group("/member")
	memberController := new(controllers.MemberController)
	member.Get("/", memberController.List)
	member.Post("/", memberController.Add)

}
